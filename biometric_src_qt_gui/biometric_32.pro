#-------------------------------------------------
#
# Project created by QtCreator 2014-07-16T23:41:33
#
#-------------------------------------------------


QT       += core gui
QT += multimedia
QT += multimediawidgets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += 32bit

TEMPLATE = app

TARGET = biometric_32bit

SOURCES += main.cpp\
        biometric.cpp \
    dialog.cpp

HEADERS  += biometric.h \
    dialog.h

FORMS    += biometric.ui\
    dialog.ui


INCLUDEPATH += C:/openCV_245/build/include

LIBS += -lC:/openCV_245/build/x86/vc11/lib/opencv_calib3d245 \
-lC:/openCV_245/build/x86/vc11/lib/opencv_contrib245 \
-lC:/openCV_245/build/x86/vc11/lib/opencv_core245 \
-lC:/openCV_245/build/x86/vc11/lib/opencv_features2d245 \
-lC:/openCV_245/build/x86/vc11/lib/opencv_flann245 \
-lC:/openCV_245/build/x86/vc11/lib/opencv_gpu245 \
-lC:/openCV_245/build/x86/vc11/lib/opencv_highgui245 \
-lC:/openCV_245/build/x86/vc11/lib/opencv_imgproc245 \
-lC:/openCV_245/build/x86/vc11/lib/opencv_legacy245 \
-lC:/openCV_245/build/x86/vc11/lib/opencv_ml245 \
-lC:/openCV_245/build/x86/vc11/lib/opencv_nonfree245 \
-lC:/openCV_245/build/x86/vc11/lib/opencv_objdetect245 \
-lC:/openCV_245/build/x86/vc11/lib/opencv_photo245 \
-lC:/openCV_245/build/x86/vc11/lib/opencv_stitching245 \
-lC:/openCV_245/build/x86/vc11/lib/opencv_superres245 \
-lC:/openCV_245/build/x86/vc11/lib/opencv_ts245 \
-lC:/openCV_245/build/x86/vc11/lib/opencv_video245 \
-lC:/openCV_245/build/x86/vc11/lib/opencv_videostab245

