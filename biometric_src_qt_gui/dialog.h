#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>

namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent = 0);
    ~Dialog();

private slots:
    void on_cascadeFileBrowseButton_clicked();

    void on_inputDirBrowseButton_clicked();

    void on_outputDirBrowseButton_clicked();

    void on_fullOutputDirBrowseButton_clicked();

public:
    Ui::Dialog *ui;
    void initialize();
    void loadSettings();
    void saveSettings();

    int imageWidth;
    int imageHeight;
    QString inputImageExtension;
    QString outputImageExtension;
    QString inputPath;
    QString outputPath;
    QString thumbPath;
    QString fullImagePath;

    int cameraDeviceId;
    int cameraImageWidth;
    int cameraImageHeight;

    // Copy this file from opencv/data/haarscascades to target folder
    QString face_cascade_name;

    //VARIABLES END

};

#endif // DIALOG_H
