#include "biometric.h"
#include "ui_biometric.h"
#include "dialog.h"
#include <QFileDialog>
#include <QMessageBox>
#include <QSettings>

#include <iostream>
#include <stdio.h>
#include <fstream>

double cameraScale=1.0;
cv::CascadeClassifier face_cascade;
Dialog *settingsDialog;


QImage BioMetric::scale(const QString &imageFileName)
{
    QImage image(imageFileName);
    return image.scaled(QSize(settingsDialog->imageWidth, settingsDialog->imageHeight), Qt::KeepAspectRatio, Qt::SmoothTransformation);
}


BioMetric::BioMetric(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::BioMetric)
{
    ui->setupUi(this);
    rollNumber="";
    settingsDialog=new Dialog(this);

    //saveSettings();
    settingsDialog->loadSettings();

    updateUI();

}

BioMetric::~BioMetric()
{
    delete ui;
}

void BioMetric::on_actionSettings_triggered()
{
    settingsDialog->initialize();
    switch(settingsDialog->exec())
    {
    case settingsDialog->Accepted:

        settingsDialog->saveSettings();
        QMessageBox::information(this, tr("Alert"),tr("Settings Updated"));
        updateUI();

        break;

    case settingsDialog->Rejected:
        break;
    }

}

void BioMetric::on_actionExit_triggered()
{
    QApplication::quit();
}

void BioMetric::on_actionAbout_triggered()
{
    QMessageBox::information(this, tr("About Us"),tr("Pratik Varshney"));
}


void BioMetric::on_goButton_clicked()
{
    rollNumber = (ui->rollNumberLineEdit->text());
    QString filePath = settingsDialog->inputPath+rollNumber+settingsDialog->inputImageExtension;
    QImage image=scale(filePath);
    if(image.isNull())ui->dbImageLabel->setText("Roll Number "+rollNumber+" is not present in our database");
    else ui->dbImageLabel->setPixmap(QPixmap::fromImage(image));

    refreshImage();

}

void BioMetric::on_rollNumberLineEdit_returnPressed()
{
    //on_goButton_clicked();
}

void BioMetric::on_rollNumberLineEdit_editingFinished()
{
    on_goButton_clicked();
}


void BioMetric::on_cameraButton_clicked()
{
    if(rollNumber.length()>0)
    {
        ui->imageLabel->setText("Loading Camera ...");
        crop_image();
    }
    else
        QMessageBox::information(this, tr("Alert"),tr("Please Enter a roll Number."));

}


void BioMetric::refreshImage()
{
    QString filePath = settingsDialog->outputPath+rollNumber+settingsDialog->outputImageExtension;
    QImage image=scale(filePath);
    if(image.isNull())ui->imageLabel->setText("Click on camera to capture the image");
    else ui->imageLabel->setPixmap(QPixmap::fromImage(image));

}

using namespace std;
using namespace cv;
void BioMetric::crop_image()
{

    ui->imageLabel->setText("Cropping ...");



    // Load the cascade
    if (!face_cascade.load(settingsDialog->face_cascade_name.toStdString()))
    {
        printf("--(!)Error loading\n");
        QMessageBox::information(this, tr("Error"),tr("--(!)Error loading cascade file"));
        return;
    };
    VideoCapture cap (settingsDialog->cameraDeviceId);
    cap.set(CV_CAP_PROP_FRAME_HEIGHT,settingsDialog->cameraImageHeight);
    cap.set(CV_CAP_PROP_FRAME_WIDTH,settingsDialog->cameraImageWidth);

    if(settingsDialog->cameraImageHeight > settingsDialog->cameraImageWidth)
    {
        cameraScale = settingsDialog->cameraImageHeight/640;
    }
    else
    {
        cameraScale = settingsDialog->cameraImageWidth/640;
    }

    Mat frame;

    namedWindow("original", CV_WINDOW_NORMAL);
    setWindowProperty("original", CV_WND_PROP_FULLSCREEN, CV_WINDOW_FULLSCREEN);
    int c;

    while(1)
    {

        // Read the image file
    cap>>frame;


      // Apply the classifier to the frame
        if (!frame.empty())
        {


            c=detectAndDisplay(frame);

        }
        else
        {
            printf(" --(!) No captured frame -- Break!");
            break;
        }



        if (27 == char(c))
        {
            destroyWindow("original");
            break;
        }


    }
    cap.release();


    refreshImage();
}

// Function detectAndDisplay
int BioMetric::detectAndDisplay(cv::Mat frame_full)
{

    std::vector<Rect> faces;
    Mat frame;
    Mat frame_gray;
    Mat crop;
    cv::resize(frame_full, frame, Size(settingsDialog->cameraImageWidth/cameraScale, settingsDialog->cameraImageHeight/cameraScale), 0, 0, INTER_LINEAR);
    Mat displayframe=frame_full.clone();

    cv::cvtColor(frame, frame_gray, COLOR_BGR2GRAY);
    cv::equalizeHist(frame_gray, frame_gray);

// Detect faces
    face_cascade.detectMultiScale(frame_gray, faces, 1.1, 3, 0 | CASCADE_SCALE_IMAGE, Size(100,100));

// Set Region of Interest
    cv::Rect roi_b;
    cv::Rect roi_c;

    size_t ic = 0; // ic is index of current element
    int ac = 0; // ac is area of current element

    size_t ib = 0; // ib is index of biggest element
    int ab = 0; // ab is area of biggest element

    for (ic = 0; ic <faces.size() ; ic++) // Iterate through all current elements (detected faces)

    {
        roi_c.x = faces[ic].x;
        roi_c.y = faces[ic].y;
        roi_c.width = (faces[ic].width);
        roi_c.height = (faces[ic].height);

        ac = roi_c.width * roi_c.height; // Get the area of current element (detected face)

        roi_b.x = ((faces[ib].x));
        roi_b.y = ((faces[ib].y));
        roi_b.width = (faces[ib].width);
        roi_b.height = ((faces[ib].height));



        ab = roi_b.width * roi_b.height; // Get the area of biggest element, at beginning it is same as "current" element

        if (ac > ab)
        {
            ib = ic;
        roi_b.x = ((faces[ib].x));
        roi_b.y = ((faces[ib].y));
        roi_b.width = ((faces[ib].width));
        roi_b.height = ((faces[ib].height));
        }
    }

    roi_b.x = (roi_b.x)-(roi_b.width)/2;
    roi_b.y = (roi_b.y)-(roi_b.height)/2;


    if(roi_b.x <0)
        roi_b.x =0;
    if(roi_b.y<0)
        roi_b.y =0;

    roi_b.width=floor((roi_b.width)*(2));
    roi_b.height= floor((roi_b.height)*(2));

    if(roi_b.height>(frame.rows)-(roi_b.y))
        {
            roi_b.height=(frame.rows)-(roi_b.y);
        }
        if(roi_b.width > (frame.cols)- (roi_b.x))
        {
            roi_b.width= (frame.cols)- (roi_b.x);
        }



        roi_b.x = floor((roi_b.x)*(cameraScale));
        roi_b.y =floor((roi_b.y)*(cameraScale));
        roi_b.width=floor((roi_b.width)*(cameraScale));
        roi_b.height= floor((roi_b.height)*(cameraScale));

        cv::rectangle(displayframe, roi_b, CV_RGB(255, 255, 0), 2, CV_AA);

        crop = frame_full(roi_b);

        int fontface = cv::FONT_HERSHEY_COMPLEX_SMALL;
        double scale = 1;
        int thickness = 1;
        int baseline = 0;
        string label="Press Enter to capture image (ESC to cancel)";

        cv::Size text = cv::getTextSize(label, fontface, scale, thickness, &baseline);
        cv::rectangle(displayframe, cvPoint(10, 10), cvPoint(displayframe.cols-20, text.height+20), cvScalar(0, 0, 0), CV_FILLED, CV_AA);

        cv::putText(displayframe, label, cvPoint(30, 30), FONT_HERSHEY_COMPLEX_SMALL, 1, cvScalar(0, 0, 255), 1, CV_AA);


    cv::imshow("original", displayframe);
    int c = waitKey(33);



    if (13 == char(c))
    {
        QString filename_check = settingsDialog->outputPath+rollNumber+settingsDialog->outputImageExtension;
        string filename = (filename_check).toStdString();

        QFile checkfile(filename_check);
        if(checkfile.exists())
        {
            string message = "File "+filename+" Already Exist.\nDo you want to overwtite it ?";

            QMessageBox::StandardButton reply;
            reply = QMessageBox::question(this, tr("File Overwrite"),
                                            tr(message.c_str()),
                                            QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel);
            if (reply != QMessageBox::Yes)
                return 27;
        }
        if(faces.size()>0)
        {
             cv::imwrite(filename,crop );
        }
        else
        {
             cv::imwrite(filename,frame_full );
             QMessageBox::information(this, tr("Warning"),tr("Unable to detect face. Please try again"));

        }
        filename = (settingsDialog->fullImagePath+rollNumber+".jpg").toStdString();
        cv::imwrite(filename,frame_full );
        cv::destroyWindow("original");
        return 27;

    }

   return c;
}



void BioMetric::on_compareButton_clicked()
{
    string command = "br -algorithm FaceRecognition -compare "+(settingsDialog->inputPath+rollNumber+settingsDialog->inputImageExtension).toStdString()+" "+(settingsDialog->outputPath+rollNumber+settingsDialog->outputImageExtension).toStdString()+" temp.csv";
    system(command.c_str());

    std::ifstream file("temp.csv", ifstream::in);
    if (!file) {
        string error_message = "No valid input file was given, please check the given filename.";
        CV_Error(CV_StsBadArg, error_message);
    }
    string line, val;
    getline(file, line);
    getline(file, line);
    val=line.substr(line.rfind(',')+1);
    val=format("%.2f",atof(val.c_str())*100);


    QMessageBox::information(this, tr("Results (Beta)"),tr(("Match : "+val+"%").c_str()));

}


void BioMetric::updateUI()
{
    ui->dbImageLabel->setFixedWidth(settingsDialog->imageWidth);
    ui->dbImageLabel->setFixedHeight(settingsDialog->imageHeight);
    ui->imageLabel->setFixedWidth(settingsDialog->imageWidth);
    ui->imageLabel->setFixedHeight(settingsDialog->imageHeight);
    ui->capturedImgGroupBox->setFixedWidth(settingsDialog->imageWidth+50);
    ui->capturedImgGroupBox->setFixedHeight(settingsDialog->imageHeight+50);
    ui->dbImgGroupBox->setFixedWidth(settingsDialog->imageWidth+50);
    ui->dbImgGroupBox->setFixedHeight(settingsDialog->imageHeight+50);

    on_goButton_clicked();
}



void BioMetric::on_thumbButton_clicked()
{
    string filename = (settingsDialog->thumbPath+rollNumber).toStdString();
    string command="java -jar EnrollmentSample.jar "+filename;
    system(command.c_str());
}
