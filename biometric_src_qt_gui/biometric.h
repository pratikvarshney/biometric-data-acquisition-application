#ifndef BIOMETRIC_H
#define BIOMETRIC_H

#include <QMainWindow>
#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

namespace Ui {
class BioMetric;
}

class BioMetric : public QMainWindow
{
    Q_OBJECT

public:
    explicit BioMetric(QWidget *parent = 0);
    ~BioMetric();

private slots:
    void on_actionSettings_triggered();

    void on_actionExit_triggered();

    void on_actionAbout_triggered();

    void on_goButton_clicked();

    void on_rollNumberLineEdit_returnPressed();

    void on_cameraButton_clicked();

    void refreshImage();

    void on_compareButton_clicked();


    void on_thumbButton_clicked();

    void on_rollNumberLineEdit_editingFinished();

private:
    Ui::BioMetric *ui;
    void crop_image();
    int detectAndDisplay(cv::Mat frame);
    void updateUI();
    QImage scale(const QString &imageFileName);

    QString rollNumber;
};

#endif // BIOMETRIC_H
