#include "dialog.h"
#include "ui_dialog.h"
#include <QFileDialog>
#include <QSettings>

QSettings settings("settings_biometric.ini", QSettings::IniFormat);

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);
    thumbPath="thumb\\";

    /*imageWidth = 500;
    imageHeight = 500;

    inputImageExtension = ".bmp";
    outputImageExtension = ".bmp";
    inputPath="input\\";
    outputPath="output\\";
    fullImagePath="full\\";

    cameraDeviceId=0;
    cameraImageWidth=1280;
    cameraImageHeight=720;

    face_cascade_name = "haarcascade_frontalface_alt.xml";*/

    loadSettings();
}

Dialog::~Dialog()
{
    delete ui;
}

void Dialog::initialize()
{
    ui->imgAreaWidthSpinBox->setValue(imageWidth);
    ui->imgAreaHeightSpinBox->setValue(imageHeight);
    ui->inputExtensionComboBox->setCurrentIndex(ui->inputExtensionComboBox->findText(inputImageExtension));
    ui->outputExtensionComboBox->setCurrentIndex(ui->outputExtensionComboBox->findText(outputImageExtension));
    ui->inputDirLineEdit->setText(inputPath);
    ui->outputDirLineEdit->setText(outputPath);
    ui->fullOutputDirLineEdit->setText(fullImagePath);
    ui->cameraIDSpinBox->setValue(cameraDeviceId);
    ui->cameraImgWidthSpinBox->setValue(cameraImageWidth);
    ui->cameraImgHeightSpinBox->setValue(cameraImageHeight);
    ui->cascadeFileLineEdit->setText(face_cascade_name);
}

void Dialog::saveSettings()
{

    imageWidth = ui->imgAreaWidthSpinBox->value();
    imageHeight = ui->imgAreaHeightSpinBox->value();
    inputImageExtension = ui->inputExtensionComboBox->currentText();
    outputImageExtension = ui->outputExtensionComboBox->currentText();
    inputPath = ui->inputDirLineEdit->text();
    outputPath = ui->outputDirLineEdit->text();
    fullImagePath = ui->fullOutputDirLineEdit->text();
    cameraDeviceId = ui->cameraIDSpinBox->value();
    cameraImageWidth = ui->cameraImgWidthSpinBox->value();
    cameraImageHeight = ui->cameraImgHeightSpinBox->value();
    face_cascade_name = ui->cascadeFileLineEdit->text();

    settings.setValue("imageWidth", imageWidth);
    settings.setValue("imageHeight", imageHeight);
    settings.setValue("inputImageExtension", inputImageExtension);
    settings.setValue("outputImageExtension", outputImageExtension);
    settings.setValue("inputPath", inputPath);
    settings.setValue("outputPath", outputPath);
    settings.setValue("fullImagePath", fullImagePath);
    settings.setValue("cameraDeviceId", cameraDeviceId);
    settings.setValue("cameraImageWidth", cameraImageWidth);
    settings.setValue("cameraImageHeight", cameraImageHeight);
    settings.setValue("face_cascade_name", face_cascade_name);

}

void Dialog::loadSettings()
{
    imageWidth=settings.value("imageWidth",imageWidth).toInt();
    imageHeight=settings.value("imageHeight", imageHeight).toInt();
    inputImageExtension=settings.value("inputImageExtension", inputImageExtension).toString();
    outputImageExtension=settings.value("outputImageExtension", outputImageExtension).toString();
    inputPath=settings.value("inputPath", inputPath).toString();
    outputPath=settings.value("outputPath", outputPath).toString();
    fullImagePath=settings.value("fullImagePath", fullImagePath).toString();
    cameraDeviceId=settings.value("cameraDeviceId", cameraDeviceId).toInt();
    cameraImageWidth=settings.value("cameraImageWidth", cameraImageWidth).toInt();
    cameraImageHeight=settings.value("cameraImageHeight", cameraImageHeight).toInt();
    face_cascade_name=settings.value("face_cascade_name", face_cascade_name).toString();

}

void Dialog::on_cascadeFileBrowseButton_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this,
                                                    tr("Open File"), QDir::currentPath(), tr("XML files (*.xml)"));
    if (!fileName.isEmpty())
    {
        ui->cascadeFileLineEdit->setText(fileName);
    }
}

void Dialog::on_inputDirBrowseButton_clicked()
{
    QString directory = QFileDialog::getExistingDirectory(this,
                               tr("Select Directory"), QDir::currentPath());

    if (!directory.isEmpty())
    {
        ui->inputDirLineEdit->setText(directory+"/");
    }
}

void Dialog::on_outputDirBrowseButton_clicked()
{
    QString directory = QFileDialog::getExistingDirectory(this,
                               tr("Select Directory"), QDir::currentPath());

    if (!directory.isEmpty())
    {
        ui->inputDirLineEdit->setText(directory+"/");
    }
}

void Dialog::on_fullOutputDirBrowseButton_clicked()
{
    QString directory = QFileDialog::getExistingDirectory(this,
                               tr("Select Directory"), QDir::currentPath());

    if (!directory.isEmpty())
    {
        ui->fullOutputDirLineEdit->setText(directory+"/");
    }
}
