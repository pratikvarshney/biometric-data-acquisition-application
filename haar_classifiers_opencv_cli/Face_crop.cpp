#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

    #include <iostream>
    #include <stdio.h>

    using namespace std;
    using namespace cv;

    // Function Headers
    void detectAndDisplay(Mat frame);

    // Global variables
	int num=1;
    // Copy this file from opencv/data/haarscascades to target folder
    string face_cascade_name = "lbp_200_32x16.xml";
    CascadeClassifier face_cascade;
    string window_name = "Capture - Face detection";
    int filenumber; // Number of file to be saved
    String filename;
	int i;

    // Function main
    int main(void)
    {
        // Load the cascade
        if (!face_cascade.load(face_cascade_name))
        {
            printf("--(!)Error loading\n");
            int ch = std::cin.get();
            return (-1);
        };
		Mat frame;
		char name[100];
		for(i=5613149;i<=5652111;i++)
		{
			sprintf(name,"../../Face_Crop/forms/%d 001.jpg",i);
			// Read the image file
        frame = imread(name,1);
		if(!frame.empty())
		{
/*	//VideoCapture cap("C:/Users/bajaj/Videos/Documents/Visual Studio 2012/Projects/Face_Crop/forms/%03d.jpg");
//while(cap.isOpened())
//	{
	//cap.read(frame); */
		for(int j=0;j<1;j++)
		{
      
          // Apply the classifier to the frame
            if (!frame.empty())
            {
                detectAndDisplay(frame);
            }
            else
            {
                printf(" --(!) No captured frame -- Break!");
                int ch = std::cin.get();
                break;
            }

            int c = waitKey(10);

            if (27 == char(c))
            {
                break;
            }
        }
		}
		}
	getchar();

        return 0;
    }
	
    // Function detectAndDisplay
    void detectAndDisplay(Mat frame)
    {
        std::vector<Rect> faces;
        Mat frame_gray;
        Mat crop;
        Mat res;
        Mat gray;
        string text;
        stringstream sstm;

		cvtColor(frame, frame_gray, COLOR_BGR2GRAY);
        equalizeHist(frame_gray, frame_gray);

    // Detect faces
        face_cascade.detectMultiScale(frame_gray, faces, 1.1, 8, 0 | CASCADE_SCALE_IMAGE, Size(200,100), Size(440,220));

    // Set Region of Interest
        cv::Rect roi_b;
        cv::Rect roi_c;

        size_t ic = 0; // ic is index of current element
        int ac = 0; // ac is area of current element

        size_t ib = 0; // ib is index of biggest element
        int ab = 0; // ab is area of biggest element
		

        for (ic = 0; ic <faces.size() ; ic++) // Iterate through all current elements (detected faces)

        {
            roi_c.x = faces[ic].x;
            roi_c.y = faces[ic].y;
            roi_c.width = (faces[ic].width);
            roi_c.height = (faces[ic].height);

            ac = roi_c.width * roi_c.height; // Get the area of current element (detected face)

            roi_b.x = (faces[ib].x);
            roi_b.y = (faces[ib].y);
            roi_b.width = (faces[ib].width);
            roi_b.height = (faces[ib].height);

            ab = roi_b.width * roi_b.height; // Get the area of biggest element, at beginning it is same as "current" element

            if (ac > ab)
            {
                ib = ic;
                roi_b.x = faces[ib].x;
                roi_b.y = faces[ib].y;
                roi_b.width = (faces[ib].width);
                roi_b.height = (faces[ib].height);
            }

			

		}

		roi_b.x = roi_b.x - (roi_b.width / 8);
        roi_b.y = roi_b.y - (roi_b.height / 4);
        roi_b.width *= 1.25;
        roi_b.height *= 1.5;

		if(roi_b.x < 0)
			roi_b.x = 0;
		if(roi_b.y < 0)
			roi_b.y = 0;
		if(roi_b.width > (frame.cols - roi_b.x))
			roi_b.width = (frame.cols - roi_b.x);
		if(roi_b.height > (frame.rows - roi_b.y))
			roi_b.height = (frame.rows - roi_b.y);

		

		
		crop = frame(roi_b);
            //resize(crop, res, Size(128, 128), 0, 0, INTER_LINEAR); // This will be needed later while saving images
            //cvtColor(crop, gray, CV_BGR2GRAY); // Convert cropped image to Grayscale

            // Form a filename
            filename = "";
            stringstream ssfn;
            ssfn << i << ".bmp";
            filename = ssfn.str();
            filenumber++;

			imwrite("res\\"+filename, crop);

			cout<<num <<" croping done\n";
			num++;
			
            
			//waitKey(1000);
           
        
/*
    // Show image
        sstm << "Crop area size: " << roi_b.width << "x" << roi_b.height << " Filename: " << filename;
        text = sstm.str();

        putText(frame, text, cvPoint(30, 30), FONT_HERSHEY_COMPLEX_SMALL, 0.8, cvScalar(0, 0, 255), 1, CV_AA);
        imshow("original", frame);

        if (!crop.empty())
        {
            imshow("detected", crop);
        }
        else 
            destroyWindow("detected");  */
    }
