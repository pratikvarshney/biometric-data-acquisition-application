# README #

* GUI application for Face Detection/Extraction, Recognition and Fingerprint Detection/Extraction using OpenCV and Qt library.
* GUI is build using Qt framework.
* Face and fingerprint processing is done using OpenCV and some manually trained haar cascade classifiers.

### Note ###

* Beta release snapshot
* A part of this project uses a proprietary software, which is not included in this snapshot. 